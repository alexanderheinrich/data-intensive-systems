FROM nginx
COPY backend.nginx.conf /etc/nginx/nginx.conf
CMD ["nginx", "-g", "daemon off;"]
