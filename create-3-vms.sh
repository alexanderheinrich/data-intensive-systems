#!/bin/bash
gcloud compute instances create k8s-vm-1 --machine-type=e2-standard-2 --image-project=ubuntu-os-cloud --image=ubuntu-1804-bionic-v20211021 --tags=cloud-computing --zone=europe-west3-a
gcloud compute instances create k8s-vm-2 --machine-type=e2-standard-2 --image-project=ubuntu-os-cloud --image=ubuntu-1804-bionic-v20211021 --tags=cloud-computing --zone=europe-west3-a
gcloud compute instances create k8s-vm-3 --machine-type=e2-standard-2 --image-project=ubuntu-os-cloud --image=ubuntu-1804-bionic-v20211021 --tags=cloud-computing --zone=europe-west3-a
