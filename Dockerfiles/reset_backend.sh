#!/bin/bash
sudo docker container stop backend-container
sudo docker container rm backend-container backend-container
sudo docker rmi backend
sudo docker build -f backend.Dockerfile -t backend .
sudo docker run -dit -p 8081:80 --name backend-container -d backend /bin/bash
sudo docker ps -a
sudo docker exec -it backend-container service nginx start
