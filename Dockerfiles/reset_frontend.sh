#!/bin/bash
sudo docker container stop frontend-container
sudo docker container rm frontend-container backend-container
sudo docker rmi frontend
sudo docker build -f frontend.Dockerfile -t frontend .
sudo docker run -dit -p 8080:80 --name frontend-container -d frontend /bin/bash
sudo docker ps -a
sudo docker exec -it frontend-container service nginx start
